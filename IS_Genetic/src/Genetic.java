import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;



public class Genetic {
	
	/*
	 *  Config Variables
	 */
	static int  INITIAL_POPULATION = 100;
	static int INSTANCES_TO_CROSSOVER =50;
	static int INSTANCES_PERCENTAGE_FROM_SCHEMA =20;
	static int SIZE;
	static int SOLUTION_THRESHOLD = 0;
	static long GENERATIONS_THRESHOLD = 20000;
	static double MUTATION_RATE = 10; 
	HashSet<Integer[]> schema;
	
	
	
	/*
	 *  Basic constructor with size of the board as parameter
	 */
	public Genetic(int size) {
		
		this.SIZE = size;
		
	}
	
	/*
	 *  Constructor for command line arguments
	 */
	public Genetic(int size,int solutionThreshold,int population, int crossoverThreshold, int generations){
		
		INITIAL_POPULATION = population;
		INSTANCES_TO_CROSSOVER = crossoverThreshold;
		SIZE = size;
		SOLUTION_THRESHOLD = solutionThreshold;
		GENERATIONS_THRESHOLD = generations;
		schema= new HashSet<Integer[]>();
	}
	
	
	/*
	 *  Follows the principles of genetic algorithm and processes for solution with respect to given threshold
	 */
	public void processGenetic(boolean schemaEnabled) throws Exception{
		
		Integer[] bestSofar;
		Integer[] bestAmongstCurrentPopulation;
		ArrayList<Integer[]> genNew= new ArrayList<Integer[]>();
		String scheme = "";
		
		for(int i=0;i<SIZE;i++){
			scheme+="X";
		}
		
		for(int i=0;i<INITIAL_POPULATION;i++){
			
			if(schemaEnabled){
				genNew.add(getCombinationFromSchem(scheme));
			}else{
				genNew.add(getCombination());
			}
		}
		
		bestSofar = genNew.get(0);
		bestAmongstCurrentPopulation = genNew.get(0);
		long generations = GENERATIONS_THRESHOLD; 
		int runs =0; 
		
		while((runs<generations)&&(new Board(bestSofar).cost()>SOLUTION_THRESHOLD)){
			
			runs++;
			int genNewTotalCost = totalPopulationCost(genNew);
			bestAmongstCurrentPopulation =getBest(genNew);
			System.out.println("\n"+runs+": The total cost of population is : "+genNewTotalCost);
			System.out.println("Fittest amongst population is :"+new Board(bestAmongstCurrentPopulation).cost()+"\n");
			System.out.println(new Board(bestAmongstCurrentPopulation).print());
			
			if(new Board(bestAmongstCurrentPopulation).cost()<new Board(bestSofar).cost()){
				bestSofar = bestAmongstCurrentPopulation;
			}
			
			ArrayList<Integer> temp = new ArrayList<Integer>();
			
			try{
				Collections.sort(genNew, new costComparator());
				
				
				if(schemaEnabled){
					
					// calculate the count of instances to replace
					int instancesFromSchema = (int) ((float)((INSTANCES_PERCENTAGE_FROM_SCHEMA)*genNew.size())*(0.01));
					scheme = new Diff().diffAll(genNew.subList(0,instancesFromSchema));
					//calculate instances from  the scheme here and replace them with poor instances
					for(int i=0,j=genNew.size()-1;i<instancesFromSchema;i++,j--){
						genNew.add(j, getCombinationFromSchem(scheme));
					}
					// sort again
					Collections.sort(genNew, new costComparator());
				}
				
			}catch(Exception e){
				e.printStackTrace();
				System.exit(-1);
			}
			ArrayList<Integer[]> genOld = genNew;
			
			genNew = new ArrayList<Integer[]>();
			
			int crossoverThreshold = (int) ((double)INITIAL_POPULATION*((double)INSTANCES_TO_CROSSOVER/100));
			
			for(int i=0;i<INITIAL_POPULATION-1;i+=2){
				
				
				Integer[] one = genOld.get(i);
				Integer[] two = genOld.get(i+1);
				
				
				Integer[] childOne = one;
				Integer[] childTwo = two;
				
				
				if(i<crossoverThreshold){
					
					 childOne = new Integer[SIZE];
					 childTwo = new Integer[SIZE];
					
					int crossOverPoint = getRandom(1,SIZE-1);
					
					for(int j=0;j<SIZE;j++){
						
						if(j<crossOverPoint){
							childOne[j]=one[j];
							childTwo[j]=two[j];
						}else{
							childOne[j]= two[j];
							childTwo[j]= one[j];
						}
					}
				}
				genNew.add(i,childOne);
				genNew.add(i+1,childTwo);
			}
			
			// mutation section startes here
			for(Integer[] mutateChild: genNew){
				
				//create a temp list
				ArrayList<Integer> randomList = new ArrayList<Integer>();
				
				// number of indexes to select for mutation
				int mutateCount = (int)Math.ceil((float)SIZE*(MUTATION_RATE/100));
				
				//  empty list to add indexes 
				ArrayList<Integer> randomSet = new ArrayList<Integer>();
				
				// add indexes to the list for later to pick from random
				for(int i=0;i<SIZE;i++){
					randomSet.add(new Integer(i));
				}
				
				// select the indexes 
				for(int k=0;k<mutateCount;k++){
					randomList.add(randomSet.remove(getRandom(0, randomSet.size()-1)));
				}
				
				// use picked indexes to mutate the feature
				for(Integer currentRandom: randomList){
					mutateChild[currentRandom]= getRandom(0, SIZE-1);
				}
				
			}
			
			
	}
		
		Board best = new Board(bestSofar);
		
		if(runs<generations){
			System.out.println("\n\n[SUCCESS] Solution found in over "+runs+" generations and has cost:"+ best.cost()+"\n"+best);
		}else{
			System.out.println("\n\n[FAIL] Best result found so far has cost:"+ best.cost()+"\n"+best);
		}
		
		
	}
	
	/*
	 * to calculate the cost of selected population
	 */
	public int unitPopulationCost(int[] population){
		
		return new Board(population).cost();
	}
	
	
	/*
	 *  to calculate the cost total instances in population
	 */
	public int totalPopulationCost(ArrayList<Integer[]> total){
		int sum=0;
		
		for(int i=0;i<total.size();i++){
			
			sum+= new Board(total.get(i)).cost();
		}
		
		return sum;
	}

	/*
	 * to find the best combination out of the population
	 */
	public Integer[] getBest(ArrayList<Integer[]> total){
		
		Integer[] best = total.get(0); 
		
 		for(int i=1;i<total.size();i++){
 			
 			if(new Board(total.get(i)).cost()<new Board(best).cost()){
 				best = total.get(i);
 			}
 		}
 		
 		return best;
	}
	
	/*
	 *  Returns a random configuration for a board with respect to given size of board
	 */
	public Integer[] getCombination(){
	
		Integer[] result = new Integer[SIZE];
		
			for(int i=0;i<SIZE;i++){
				result[i]= getRandom(0, SIZE-1);
			}
		
		return result;
	}
	
	/*
	 *  Returns a random configuration for a board with respect to given size of board
	 */
	public Integer[] getCombinationFromSchem(String scheme) throws Exception{
	
		Integer[] result = new Integer[SIZE];
		if(scheme.length()==SIZE){
				for(int i=0;i<SIZE;i++){
					
					result[i]= scheme.charAt(i)=='X'?getRandom(0, SIZE-1):Integer.parseInt(scheme.charAt(i)+"");
				}
		}else{
			throw new Exception("Size of array["+SIZE+"] is unequal to Scheme length ["+scheme.length()+"]");
		}
		
		return result;
	}
	
	/*
	 * Gnerate a random number in a specific range
	 */
	public static int getRandom(int min, int max){

		if(min==max){
			return min;
		}

		Random r = new Random();
		return r.nextInt(max - min+1) + min;
	}
	
	/*
	 * 
	 *  Get a binary string for crossing over to parents	
	 */
	public static int[] getCrossOverMatrix(int size){
	
		int[] result = new int[size];
		for(int i=0;i<size;i++){
			
			result[i]= getRandom(0,1);
			
		}
		
		return result;
	}
	
	/*
	 *  Returns a sequences with given amount of mutation in it i.e, given amount of bits flipped
	 */
	public int[] mutate(int[] current, double mutationRate){
		
		int mutationLimit= (int) (current.length*(mutationRate/100));
		HashSet<Integer> selectedIndices = new HashSet<Integer>();
		ArrayList<Integer> toPickFrom = new ArrayList<Integer>();
		
		for(int i=0;i<current.length;i++){
			
			toPickFrom.add(i);
		}
		
		for(int i=0;i<mutationLimit;i++){
			
			int randomPointer = toPickFrom.remove(getRandom(0, toPickFrom.size()));
			current[randomPointer] = getRandom(0, current.length);
		}
		
		return current;
		
	}
	
	
	/*
	 *  Comparotor class in order to sort the population with respect to the cost in ascending order
	 */
	class costComparator implements Comparator<Integer[]>{
		 
	    @Override
	    public int compare(Integer[] p1, Integer[] p2) {
	    	
	    	Board b1 = new Board(p1);
	    	Board b2 = new Board(p2);
	    	
	        if(b1.cost() > b2.cost()){
	            return 1;
	        } else if(b1.cost() < b2.cost()){
	            return -1;
	        }else{
	        	return 0;
	        }
	    }
	}
	
	
	
	
	
	public static void main(String[] args) {

		Genetic test = new Genetic(8);
		String message = "Running with default ";
		
		HashSet <String> test2 = new HashSet<String>();
		
		test2.add("a");
		System.out.println(test2.contains(new String("a")));		
		
		if(args.length==6){
			int sizeOfBoard = Integer.parseInt(args[0]);
			int solutionThreshold = Integer.parseInt(args[1]);
			int population = Integer.parseInt(args[2]);
			int crossoverThreshold = Integer.parseInt(args[3]);
			int generationsToProcess = Integer.parseInt(args[4]);
			boolean enableSchema = args[5].equals("false")?false:true;
			
			
			test = new Genetic(sizeOfBoard,
					solutionThreshold,
					population,
					crossoverThreshold,
					generationsToProcess);
			message ="Running with custom commandline ";
			
		
			message = message+"paramaters with SIZE = "+Genetic.SIZE
					+" population size = "+Genetic.INITIAL_POPULATION
					+",Solution should be <= "+Genetic.SOLUTION_THRESHOLD
					+", Number of parents to crossover = "
					+Genetic.INSTANCES_TO_CROSSOVER
					+" Number of Generations to process = " +Genetic.GENERATIONS_THRESHOLD;
		 
			System.out.println(message);
			try {
				test.processGenetic(enableSchema);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println(message);
		}else{
			System.out.println("Not enough arguments, try these:\"8 0 100 50 10000 true\"");
		}
	}

}
