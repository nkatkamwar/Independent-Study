import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Diff {
	
	HashMap<String,Integer> uniqueSchemes = new HashMap<String, Integer>();
			

	public String diff(Integer[] first,Integer[] second ){
		
		String result ="";
		for(int i=0;i<first.length;i++){
			String currentResult  = first[i]-second[i]==0?first[i].toString():"X";
			result +=currentResult;
		}
		
		return result;
	}
	
	public String diffAll(List<Integer[]> listOfElements){
		
		String popularScheme ="";
		Integer popularity =0;
		for(int i=0;i<listOfElements.size();i++){
			for(int j=0;j<listOfElements.size();j++){
				if(i==j){
					continue;
				}
				
				//campare and insert in hashmap
				String newScheme = diff(listOfElements.get(i),listOfElements.get(j)); 
				if(uniqueSchemes.containsKey(newScheme)){
					uniqueSchemes.put(newScheme, uniqueSchemes.get(newScheme)+1);
				}else{
					uniqueSchemes.put(newScheme, 1);
				}
				
				if(popularity<(uniqueSchemes.get(newScheme))){
					popularity = uniqueSchemes.get(newScheme);
					popularScheme = newScheme;
				}
				
			}
		}
		
		return popularScheme;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ArrayList<Integer[]> test = new ArrayList<Integer[]>();
		
		test.add(new Integer[]{1,2,3,4,5});
		test.add(new Integer[]{5,3,1,4,5});
//		test.add(new Integer[]{2,2,3,4,4});
//		test.add(new Integer[]{1,2,3,4,5});
//		test.add(new Integer[]{1,2,3,4,5});
//		test.add(new Integer[]{1,2,3,4,5});
		
		System.out.println(new Diff().diffAll(test));
		
		Genetic testG = new Genetic(8);
		String message = "Running with default ";
		
		try {
			System.out.println(new Board(8).arrayToString(testG.getCombinationFromSchem("X2X45XX8")));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
