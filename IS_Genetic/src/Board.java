import java.awt.Point;


public class Board {
	
	int size;
	String[][] theBoard;
	Point[] allQueens;
	public static String QUEEN_SYMBOL = "x";
	int[] queensInColumn;
	
	
	public Board(int size) {
		this.size=size;
		theBoard = new String [size][size];
		for(int i=0;i<size;i++){
			for(int j=0;j<size;j++){
				
				theBoard[i][j]="_"; 
				
			}
		}
		
		allQueens = new Point[size];
	}

	public Board(int[] givenQueens){
		this.size = givenQueens.length;
		theBoard = new String [size][size];
		for(int i=0;i<size;i++){
			for(int j=0;j<size;j++){
				
				theBoard[i][j]="_"; 
				
			}
		}
		
		allQueens = new Point[size];
		
		setQueens(givenQueens);
	}
	
	public Board(Integer[] givenQueens){
		this.size = givenQueens.length;
		theBoard = new String [size][size];
		for(int i=0;i<size;i++){
			for(int j=0;j<size;j++){
				
				theBoard[i][j]="_"; 
				
			}
		}
		
		allQueens = new Point[size];
		
		setQueens(integerToint(givenQueens));
	}
	
	/*
	 * Sets the cost as integer value in a board representation
	 */
	public boolean setCost(int x,int y, int cost){
		
		if((x<size)&&(y<size)){
			theBoard[x][y] = cost+"";
			return true;
		}
		return false;
	}
	
	/*
	 * To set the queen in a position on the board
	 */
	public boolean setQueen(int x,int y) throws Exception{
		
		if((x<size)&&(y<size)){
			theBoard[x][y] = QUEEN_SYMBOL;
			allQueens[y]= new Point(x,y);
			return true;
		}else{
			throw new Exception("Coordinates out of bound, Size = "+size+" and given coordinate is "+x+", "+y);
		}
	}
	
	/*
	 *  To set all the queens at once for a given board
	 */
	public boolean setQueens(int[] allQueens){
		try{
			for(int i=0;i<size;i++){
				if(!setQueen(allQueens[i],i)){
					return false;
				}
			}
			queensInColumn = allQueens;
		}catch(Exception e){
			e.printStackTrace();
			System.exit(-1);
		}	
			return true;
	}
	
	/*
	 *  Utility method to change the type of Integer [] to int []
	 */
	public int[] integerToint(Integer[] temp){
		
		int[] result= new int[temp.length];
		
		for(int i=0;i<temp.length;i++){
			result[i]=temp[i];
		}
		
		return result;
	}
	
	/*
	 *  Getter method for queens positions on the board with columns as index of the returned array
	 */
	public int[] getQueensByColumn(){
		
		return queensInColumn;
	}
	
	/*
	 *  Calculates the cost/ fitness of the current configuration, cost = number of queen pairs attacking each other directly or indirectly
	 */
	public int cost(){
		int cost =0;
		for(int i=0;i<allQueens.length-1;i++){
		
			for(int j= i+1;j<allQueens.length;j++){
				
				if(canKill(allQueens[i], allQueens[j])){
					cost++;
				}
				
			}
			
		}
		return cost;
	}
	
	/*
	 * Given two Points, returns if queens on these positions can kill each other or not
	 */
	public boolean canKill(Point one, Point two){
		
		if(one.getX()==two.getX()||one.getY()==two.getY()){
			return true;
		}else if(Math.abs((one.getX()-two.getX()))==Math.abs((one.getY()-two.getY()))){
			return true;
		}
		return false;
	}
	
	/*
	 *  Returns just the board config as String
	 */
	
	public String print(){
		
		String result = " ";
		
		for(int i=0;i<size;i++){
			result+=" "+i;
		}
		
		
		for(int i=0;i<size;i++){
			result+="\n"+i+"|";
			for(int j=0;j<size;j++){
					result+=theBoard[i][j]+"|";
			}
		}
		
		return result;
	}
	
	/*
	 * Returns the current config information and also the representation
	 */
	@Override
	public String toString(){
		
		return arrayToString(queensInColumn)+"\n\n"+print();
	}
	
	/*
	 * Prints a 1-D array to Console
	 */
	public String arrayToString(int[] array){

		String result="";
		for(int i=0;i<array.length;i++){
			result+=array[i]+",";
		}

		return (result);
	}

	/*
	 * Prints a 1-D array to Console
	 */
	public String arrayToString(Integer[] array){

		String result="";
		for(int i=0;i<array.length;i++){
			result+=array[i]+",";
		}

		return (result);
	}

	/*
	 * Prints a 2-D array to Console
	 */
	public  String arrayToString(int[][] array){

		String result="";
		for(int i=0;i<array[0].length;i++){
			result=arrayToString(array[i])+"\n";
			System.out.println();
		}

		return(result);
	}

	
	
	public static void main(String[] args){
		
		Point a = new Point(2,1);
		Point b = new Point(1,2);
		
		// tests
		
		Board test = new Board(5);
		System.out.println(test.canKill(a, b));
		try {
			test.setQueen(2, 9);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int[] testSet = {1,3,4,5,2};
		test.setQueens(testSet);
		System.out.println(test.print());
		System.out.println(test.cost());
		
	}
}
