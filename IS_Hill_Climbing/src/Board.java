import java.awt.Point;


public class Board {
	
	int size;
	String[][] theBoard;
	Point[] allQueens;
	public static String QUEEN_SYMBOL = "x";
	int[] queensInColumn;
	
	
	public Board(int size) {
		this.size=size;
		theBoard = new String [size][size];
		for(int i=0;i<size;i++){
			for(int j=0;j<size;j++){
				
				theBoard[i][j]="_"; 
				
			}
		}
		
		allQueens = new Point[size];
	}

	public Board(int[] givenQueens){
		this.size = givenQueens.length;
		theBoard = new String [size][size];
		for(int i=0;i<size;i++){
			for(int j=0;j<size;j++){
				
				theBoard[i][j]="_"; 
				
			}
		}
		
		allQueens = new Point[size];
		
		setQueens(givenQueens);
	}
	
	public Board(Integer[] givenQueens){
		this.size = givenQueens.length;
		theBoard = new String [size][size];
		for(int i=0;i<size;i++){
			for(int j=0;j<size;j++){
				
				theBoard[i][j]="_"; 
				
			}
		}
		
		allQueens = new Point[size];
		
		setQueens(integerToint(givenQueens));
	}
	
	public boolean setCost(int x,int y, int cost){
		
		if((x<size)&&(y<size)){
			theBoard[x][y] = cost+"";
			return true;
		}
		return false;
	}
	
	public boolean setQueen(int x,int y){
		
		if((x<size)&&(y<size)){
			theBoard[x][y] = QUEEN_SYMBOL;
			allQueens[y]= new Point(x,y);
			return true;
		}
		return false;
	}
	
	public boolean setQueens(int[] allQueens){
		
		for(int i=0;i<size;i++){
			if(!setQueen(allQueens[i],i)){
				return false;
			}
		}
		queensInColumn = allQueens;
		
		return true;
	}
	
	public int[] integerToint(Integer[] temp){
		
		int[] result= new int[temp.length];
		
		for(int i=0;i<temp.length;i++){
			result[i]=temp[i];
		}
		
		return result;
	}
	
	public int[] getQueensByColumn(){
		
		return queensInColumn;
	}
	
	public int cost(){
		int cost =0;
		for(int i=0;i<allQueens.length-1;i++){
		
			for(int j= i+1;j<allQueens.length;j++){
				
				if(canKill(allQueens[i], allQueens[j])){
					cost++;
				}
				
			}
			
		}
		return cost;
	}
	
	public int[][] costOfBoard(Board current){
		
		int[][] cost = new int[current.size][current.size];
		int[] temp = current.getQueensByColumn().clone();
		int tempCost =1; 
		for(int w=0;w<size;w++){

			for(int j=0;j<size;j++){

				if(temp!=null){
					Board nextBoard = new Board(size);
					temp = current.getQueensByColumn().clone();
					temp[w] = j;
//					printArray(temp);
					nextBoard.setQueens(temp);
					tempCost = nextBoard.cost();
					cost[j][w] = tempCost;
					current.setCost(w, j, tempCost);
//					System.out.println("Cost = "+ tempCost);
//					min = min<tempCost?min:tempCost;
//					answer = min<tempCost?answer:temp;
				}
			}
		}
		
		return cost;
	}
	
	public int[][] costOfBoard(){
		
		return costOfBoard(this);
	}
	
	public boolean canKill(Point one, Point two){
		
		if(one.getX()==two.getX()||one.getY()==two.getY()){
			return true;
		}else if(Math.abs((one.getX()-two.getX()))==Math.abs((one.getY()-two.getY()))){
			return true;
		}
		return false;
	}
	
	public String print(){
		
		String result = " ";
		
	//top line
		for(int i=0;i<size;i++){
			result+=" "+i;
		}
		
		
		for(int i=0;i<size;i++){
			result+="\n"+i+"|";
			for(int j=0;j<size;j++){
					result+=theBoard[i][j]+"|";
			}
		}
		
		return result;
	}
	
	/**
	 * To override toString in sys out
	 */
	public String toString(){
		String result = "[";
		for(int i :queensInColumn){
			result+=(" "+i);
		}
		result+=" ]";
		
		return result;
	}
	
	
	
	public static void main(String[] args){
		
		Point a = new Point(2,1);
		Point b = new Point(1,2);
		
		
		
		Board test = new Board(8);
		System.out.println(test.canKill(a, b));
//		test.setQueen(2, 9);
//		int[] testSet = {1,3,4,5,2,7,6,3};
		int[] testSet = {4,5,6,3,4,5,6,5};
		test.setQueens(testSet);
		System.out.println(test.print());
		System.out.println(Math.ceil((float)60/100));
		
	}
}
