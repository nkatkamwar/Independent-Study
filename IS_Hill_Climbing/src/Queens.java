import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;


public class Queens {

	Board current;
	int[][] cost;
	int size;
	static int lineNUmber = 0;
	/*
	 * Constructor which configures the board with int[] of locations of queen as initial setup
	 */
	public Queens(int[] allQueens) {

		current = new Board(allQueens.length);
		current.setQueens(allQueens);
		cost = new int[allQueens.length][allQueens.length];
		size = allQueens.length;
	}
	
	

	/*
	 * Constructor which configures the board size of the board
	 */
	public Queens(int size) {

		current = new Board(size);
		cost = new int[size][size];
		this.size =size;
	}


	
	public void generateCostSearchSpace(){
		
		
	}
	
	/*
	 * Hill climbing is realized here
	 */
	public int[] steepestHillClimb(int[] board, boolean flag){
		int[] result = new int[size];
		
		
		// calcualte cost if 0 is the solution 
			// if not, 
				// pick 1st queen, + - calc cost for both, pick best, repeat for next queen
		
			for(int i=0;i<board.length;i++){
				
				if(new Board(board).cost()==0){
					return board;
				}
				
				// pick the comaprison indexes to calculate the best move for current queen
				int topIndex = (board[i]-1)>=0?board[i]-1:board[i];
				int bottomIndex = (board[i]+1)<board.length?board[i]+1:board[i];
				
				int[] top = board.clone();
				top[i]=topIndex;
				int[] bottom = board.clone();
				bottom[i]=bottomIndex;
				
				if(new Board(top).cost()<new Board(board).cost()){
					board = flag?top:bottom;
				}else if(new Board(bottom).cost()<new Board(board).cost()){
					board = flag?bottom:top;
				}
				
			}
			
			return board;
	}
	
	
	/**
	 * This approach is stochastic with random restart if stuck until solution is found
	 * @param startingConfig
	 * @param solution
	 * @return
	 * @throws IOException 
	 */
	
	public int[] stochasticHillClimb(int[] startingConfig, int solution) throws IOException{
	
		// run steepest climb on the state if stuck at a local minima, randomly select a new config and restart until solution is found.
		
		if(new Board(startingConfig).cost()==solution){
			return startingConfig;
		}
		
		int count =0;
		while(new Board(startingConfig).cost()!=solution){
			startingConfig = steepestHillClimb(getRandomConfig(startingConfig),true);
//			System.out.println(new Board(startingConfig)+" : "+new Board(startingConfig).cost());
			count++;
			
		}
//		PrintWriter writer = new PrintWriter(new FileWriter("stochastic.csv", true)); 
//		writer.println(""+(++lineNUmber)+","+count);
//		writer.close();
		System.out.println("Result found in "+count);
		return startingConfig;
	}
	
	/**
	 * To generate random config int[]
	 * @param currentConfig
	 * @return
	 */
	public int[] getRandomConfig(int[] currentConfig){
		int[] result = currentConfig;
		
		for(int i=0;i<result.length;i++){
			int randomNum = (int)(Math.random()*(result.length));
			result[i]= randomNum;
		}
		
		return result;
	}
	
	/**
	 * Hill climbing is realized here
	 * @param cost
	 * @param window
	 * @param rate
	 * @return
	 * @throws IOException 
	 */
	public int[] SimulatedAnnealingHillClimb(int[] board,int rate) throws IOException{
		
		
		boolean[] randomnessFlags = new boolean[rate];
		ArrayList<Integer> randomness = new  ArrayList<Integer>();
		long time = System.currentTimeMillis();
		
		// add false objects in randomness, so that probability of picking false is more
		for(int i=0;i<randomnessFlags.length;i++){
			randomness.add(i);
		}
		int i =0;
		while(new Board(board).cost()!=0&&((i)<rate)){
			
			int random =(getRandom(0, randomness.size()));
			int random2 = (getRandom(0, 2));
			boolean flag2 = random==0;
			
			boolean flag1 = randomnessFlags[random]?true:flag2;
			board = steepestHillClimb(board.clone(),randomnessFlags[random]);
			System.out.println(new Board(board)+" : "+new Board(board).cost()+" FLAG:"+randomnessFlags[random]+" INDEX: "+random2);
			
			
			PrintWriter writer = new PrintWriter(new FileWriter("anealing_"+time+".csv", true)); 
			writer.println(""+(i++)+","+new Board(board).cost());
			writer.close();
			
			randomnessFlags[random]=true;
		}
		return board;
	}

	
	/*
	 * Gnerate a random number in a specific range
	 */
	public static int getRandom(int min, int max){

		if(min==max){
			return min;
		}

		Random r = new Random();
		return r.nextInt(max - min) + min;
	}
	
	/*
	 * Generates all comnination of configurations for the queens in a chess board
	 */
	public int[] nextCombinationOld(Integer i, int[] currentCombination){

		int[] result = currentCombination;

		if(result[i]+1<currentCombination.length){

			result[i]++;
		}else{

			while((result[i]+1)>=currentCombination.length){
				i--;
				if(i<0){
					return null;
				}
			}

			result[i]++;
			for(int j=i+1;j<currentCombination.length;j++){
				result[j]=0;
			}
			i = currentCombination.length-1; 
		}

		return result;
	}

	/*
	 * Prints a 1-D array to Console
	 */
	public void printArray(int[] array){

		String result="";
		for(int i=0;i<array.length;i++){
			result+=array[i]+",";
		}

		System.out.print(result);
	}


	/*
	 * Prints a 2-D array to Console
	 */
	public void printArray(int[][] array){

		String result="";
		for(int i=0;i<array[0].length;i++){
			printArray(array[i]);
			System.out.println();
		}

		System.out.print(result);
	}


	public static void main(String[] args) {

		int size = Integer.parseInt(args[0]);
		int iterations = Integer.parseInt(args[1]);
		int window = Integer.parseInt(args[2]);
		int rate = Integer.parseInt(args[3]);
		int[] queens= {4,5,6,3,4,5,6,5};
		
		if(size!=0){
			queens = new int[size];
			for(int i=0,j=4;i<size;i++,j++){
				queens[i] = Integer.parseInt(args[j]);
			}
		}
		
		
		Queens test = new Queens(queens);
		
		System.out.println("STEEPEST: "+new Board(test.steepestHillClimb(queens.clone(),true)).cost());
		try {
//			for(int i=0;i<100;i++){
			System.out.println("STOCHASTIC: \n"+new Board(test.stochasticHillClimb(queens.clone(), 0)).print());
//			}
			System.out.println("ANNEALING: \n"+new Board(test.SimulatedAnnealingHillClimb(queens.clone(), 1000)).print());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("\nConfiguration of queen at the beginning :");
		test.printArray(queens);
		System.out.println(" and its cost = "+test.current.cost());

	}

}
